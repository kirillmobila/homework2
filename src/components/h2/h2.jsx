import React from "react";

import './h2.css';

const H2 = (props) => {
    
    return(
        <h2 className="H2">
            {props.text}
        </h2>
    )
};

export default H2