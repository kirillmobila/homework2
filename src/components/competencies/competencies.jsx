import React from "react";

import './competencies.css';

const CompetenciesDiv = (props) => {
    
    return(
        <div className={props.classDiv}>
            <p className={props.classP}>
                {props.text}
            </p>
        </div>
    )
};

export default CompetenciesDiv