import React from 'react';
import ProrTypes from 'prop-types';

import './avatar.css';

const Avatar = ({
    src, href, alt, className, width, height, ...attrs
}) => {

    if (!src) {
        src = `https://via.placeholder.com/${width}x${height}`;
    }

    return (
        <a href={href} target='_blank'>
        <img
            src={src}
            alt={alt}
            className={className}
            width={width}
            height={height}
            {...attrs}
        />
        </a>
    );
};

Avatar.ProrTypes = {
    src: ProrTypes.string,
    href: ProrTypes.string,
    alt: ProrTypes.string,
    className: ProrTypes.string,
    width: ProrTypes.number,
    height: ProrTypes.number
};

Avatar.defaultProps = {
    src: '',
    href: '#',
    alt: 'image name',
    className: 'circle avatar',
    width: 170,
    height: 170,
}

export default Avatar;