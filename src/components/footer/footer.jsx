import React from "react";

import './footer.css';

const Footer = (props) => {
    
    return(
        <footer>
            <ul className='footer-nav'>
                <li><a href='#'>Здесь</a></li>
                <li><a href='#'>Скоро</a></li>
                <li><a href='#'>Что-то</a></li>
                <li><a href='#'>Будет</a></li>
            </ul>
        </footer>
    )
};

export default Footer