import React from "react";
import './maincard.css'

const Maincard = (props) => {

    return(
        <div className='light-box maincard'>
            {props.avatar}       
            <h1 className='name'>{props.name}</h1>
            <p>
                {props.comment}
            </p>
        </div>
    )
};

export default Maincard