import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Avatar from './components/avatar/avatar.jsx';
import CompetenciesDiv from './components/competencies/competencies.jsx'
import H2 from './components/h2/h2.jsx';
import Footer from './components/footer/footer.jsx';
import Maincard from './components/maincard/maincard.jsx'

function App() {

  return (
    <>
      <div className='container'>
        <Maincard
          avatar={  
            <Avatar
              href='https://youtu.be/SkEwPOezkwQ?si=0eDPB4HTzrukoLg2'
              src='src/components/imageforcard.jpg'
            />}
          name={'Кныш Кирилл Сергеевич'}
          comment={'Хочет стать прогером, но пока только хочет'}
        />
        <H2
          text={'Мои компетенции'}
        />
        <div className='grid'>
          <CompetenciesDiv
          classDiv={'competencies-card'}
          classP={'competencies'}
          text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, itaque magnam harum placeat distinctio molestias molestiae autem explicabo porro et consectetur suscipit ut ducimus accusantium officia fugiat? Hic, corrupti inventore!'}
          />
          <CompetenciesDiv
          classDiv={'competencies-card'}
          classP={'competencies'}
          text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, itaque magnam harum placeat distinctio molestias molestiae autem explicabo porro et consectetur suscipit ut ducimus accusantium officia fugiat? Hic, corrupti inventore!'}
          />
          <CompetenciesDiv
          classDiv={'competencies-card'}
          classP={'competencies'}
          text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, itaque magnam harum placeat distinctio molestias molestiae autem explicabo porro et consectetur suscipit ut ducimus accusantium officia fugiat? Hic, corrupti inventore!'}
          />
          <CompetenciesDiv
          classDiv={'competencies-card'}
          classP={'competencies'}
          text={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, itaque magnam harum placeat distinctio molestias molestiae autem explicabo porro et consectetur suscipit ut ducimus accusantium officia fugiat? Hic, corrupti inventore!'}
          />
        </div>
        <H2
          text={'Компетенции, которыми хотел бы обладать'}
        />
        <div className='light-box'>
          <div className='competencies-want'>
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
          </div>
          <div className='competencies-want'>
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
            <CompetenciesDiv
              classDiv={'competencies-card'}
              classP={'competencies'}
              text={'In nomine Domini Patris et Filii et Spiritus Sancti. Amen.'}
            />
          </div>
        </div>
      </div>
      <Footer/>
    </>
  )
}

export default App
